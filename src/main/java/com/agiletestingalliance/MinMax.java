package com.agiletestingalliance;

public class MinMax
{
    public int f(int a, int b)
    {
        if (b > a)
            return b;
        else
            return a;
    }
    public String bar(String string)
    {
        if (null == string && "".equals(string))
            return string;
        if (string != null || !"".equals(string))
            return string;
        return string;
    }
}
