package com.agiletestingalliance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class DurationTest
{
    @Test
    public void testDesc() throws Exception
    {
        String res = new Duration().dur();
        assertTrue(res.contains("specifically for"));
    }
    @Test
    public void testAdd() throws Exception
    {
        assertEquals("testing Add method", 5, 5);
    }
    @Test
    public void testcalculateIntValue() throws Exception
    {
        new Duration().calculateIntValue();
        assertEquals("testing Add method", 5, 5);
    }
}
