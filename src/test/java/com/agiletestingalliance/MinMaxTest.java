package com.agiletestingalliance;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class MinMaxTest
{
    @Test
    public void testBar1() throws Exception
    {
        String result = new MinMax().bar("Priya");
        assertEquals("Bar", "Priya", result);
    }
    @Test
    public void testBar2() throws Exception
    {
        String result = new MinMax().bar("");
        assertEquals("Bar", "", result);
    }
    @Test
    public void testBar3() throws Exception
    {
        String result = new MinMax().bar(null);
        assertEquals("Bar", null, result);
    }
    @Test
    public void testf1() throws Exception
    {
        int result = new MinMax().f(1, 2);
        assertEquals("Bar", 2, result);
    }
    @Test
    public void testf2() throws Exception
    {
        int result = new MinMax().f(4, 3);
        assertEquals("Bar", 4, result);
    }
}
